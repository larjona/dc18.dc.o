from django import forms

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from django_countries import Countries
from django_countries.fields import LazyTypedChoiceField
from django_countries.widgets import CountrySelectWidget


class AttendeeListForm(forms.Form):
    name = forms.CharField(
        label='My name',
        max_length=50,
        required=True,
    )
    country = LazyTypedChoiceField(
        label='My nationality',
        choices=Countries(),
        required=True,
        widget=CountrySelectWidget(),
    )
    opt_in = forms.BooleanField(
        label='I am willing to share this information with Taiwanese '
              'Government sponsors.',
        required=True,
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.include_media = False
        self.helper.add_input(Submit('submit', 'Submit'))
