import re

from django.core.management.base import BaseCommand

import requests

from wafer.talks.models import Talk

eventid_re = re.compile(r'^/talks/(\d+)-.*/$')

class Command(BaseCommand):
    help = 'Import published talk videos'

    def add_arguments(self, parser):
        parser.add_argument(
            '--video_base',
            default='https://meetings-archive.debian.net/pub'
                    '/debian-meetings/2018/',
            help='Base URL for videos in the archive')

    def handle(self, *args, **options):
        released = requests.get('https://sreview.debian.net/released').json()
        for video in released['videos']:
            m = eventid_re.match(video['eventid'])
            id_ = int(m.group(1))
            url = options['video_base'] + video['video']
            talk = Talk.objects.get(pk=id_)
            tu, created = talk.urls.update_or_create(
                description='Video', defaults={'url': url})

            if created:
                print('Set URL for {}'.format(talk.title))
