---
name: Lunch
---
To keep going for the rest of the day.

Served from 12:30–14:00 daily, in Restaurant 2.
