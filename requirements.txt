# Available in stretch-backports
Django>=1.11.7,<2.0

# Available in Debian
psycopg2
django-countries
python-memcached
PyYAML

# From the cheeseshop
wafer==0.7.2
django-crispy-forms>=1.7.0
django-paypal==0.4.1
